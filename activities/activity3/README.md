# Activity 3 Sheet
<b>Name:</b> Andriy Konyshev <br>
<b>Date:</b> 20240504 <br>
<b>Class:</b> CST-120 <br>
<b>Professor:</b> Bobby Estey <br><br>


## Screenshots


- **Topic Matrix Iteration 1** <br>
![Class Matrix](files_to_turn_in/matrix.png)
<br>

- **Topic Matrix Iteration 2** <br>
![table 1-3](files_to_turn_in/matrix2.png)
<br>

- **Main page iteration 1** <br>
![Table 4-6](files_to_turn_in/syllabusfirstiteration.png)
<br>

- **Main Page iteration 2** <br> 
![Table 6-7](files_to_turn_in/syllabusseconditeration.png)
<br>

- **GCU blinking red text** <br> 
![Media normal text](files_to_turn_in/blinkingtextmedia.png)
<br>

- **GCU is great rotating text** <br>
![Media SVG](files_to_turn_in/gcugreatanimation.png)
<br>

## Sololearn

- **Sololearn** <br>
![SoloLearn](files_to_turn_in/sololearncss.png)
<br>