# Activity 4 Sheet
<b>Name:</b> Andriy Konyshev <br>
<b>Date:</b> 20240519 <br>
<b>Class:</b> CST-120 <br>
<b>Professor:</b> Bobby Estey <br><br>


## Screenshots


- **Hello World java** <br>
![Class Matrix](files_to_turn_in/helloworld.png)
<br>

- **Java results** <br>
![table 1-3](files_to_turn_in/resultsjava.png)
<br>


## Sololearn

- **Sololearn** <br>
![SoloLearn](files_to_turn_in/sololearn.png)
<br>