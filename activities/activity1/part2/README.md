<h1 align="center"> Activity 1 </h1>

#### Name: Andriy Konyshev
Date: 4/28/2024 <br>
Class: CST-120 <br>
Professor: Bobby Estey <br>
<br><br>

# Website screenshots
- **Hello World**

![Hello world](https://gitlab.com/Linger/cst120/-/raw/616b76a76356f1763779ce06a175ad9d5f255006/activities/activity1/part2/images/helloworld.png) <br><br>

- **Syllabus wip**

![index page wip 1](https://gitlab.com/Linger/cst120/-/raw/main/activities/activity1/part2/images/syllabus1.png?ref_type=heads)
<br><br>

- **Syllabus page 1**

![index page wip 2 page 1](https://gitlab.com/Linger/cst120/-/raw/main/activities/activity1/part2/images/syllabus2.png?ref_type=heads)
<br><br>

- **Syllabus page 2**

![page 2](https://gitlab.com/Linger/cst120/-/raw/main/activities/activity1/part2/images/syllabus2.png?ref_type=heads)
# Code screenshots <br>
- **Hello World**

![Hello World Code](https://gitlab.com/Linger/cst120/-/raw/main/activities/activity1/part1/images/hellocode.png?ref_type=heads)
<br><br>

- **Syllabus page 1**

![index page wip 2 page 1](https://gitlab.com/Linger/cst120/-/raw/main/activities/activity1/part2/images/indexpicture.png)<br><br>

- **Syllabus page 2**

![page 2](https://gitlab.com/Linger/cst120/-/raw/main/activities/activity1/part2/images/page2code.png) <br><br>

# Sololearn screenshot

![page 2](https://gitlab.com/Linger/cst120/-/raw/main/activities/activity1/part2/images/quizcompletion.png?ref_type=heads) <br><br>