# Activity 7 Sheet
<b>Name:</b> Andriy Konyshev <br>
<b>Date:</b> 20240609 <br>
<b>Class:</b> CST-120 <br>
<b>Professor:</b> Bobby Estey <br><br>


## Screenshots


- **Professor link** <br>
![Professor link](files_to_turn_in/link.png)
<br>

- **Contact a professor PC** <br>
![contact professor form](files_to_turn_in/contact.png)
<br>

- **postman code** <br>
![Postman code](files_to_turn_in/postman.png)
<br>

- **Copyright** <br>
![Copyright](files_to_turn_in/copyright.png)
<br>

