# Activity 6 Sheet
<b>Name:</b> Andriy Konyshev <br>
<b>Date:</b> 20240602 <br>
<b>Class:</b> CST-120 <br>
<b>Professor:</b> Bobby Estey <br><br>


## Screenshots


- **Part 1 Jquery** <br>
![Class Matrix](files_to_turn_in/jqueryfirst.pngpng)
<br>

- **Part 2 Jquery** <br>
![table 1-3](files_to_turn_in/jquerysecond.png)
<br>

- **Part 3 Jquery** <br>
![table 1-3](files_to_turn_in/jquerythird.png)
<br>

- **Bootstrap** <br>
![table 1-3](files_to_turn_in/bootstrap.png)
<br>

