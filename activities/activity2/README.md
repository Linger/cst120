# Activity 2 Sheet
<b>Name:</b> Andriy Konyshev <br>
<b>Date:</b> 20240501 <br>
<b>Class:</b> CST-120 <br>
<b>Professor:</b> Bobby Estey <br><br>


## Screenshots


- **Topic Matrix**
![Class Matrix](files_to_turn_in/matrixclass.png)
<br>

- **Topic 1-3**
![table 1-3](files_to_turn_in/example1_3topics.png)
<br>

- **Topic 4-6**
![Table 4-6](files_to_turn_in/example4_6topics.png)
<br>

- **Topic 6-7**
![Table 6-7](files_to_turn_in/example6_7topics.png)
<br>

- **Media normal text**
![Media normal text](files_to_turn_in/mediaexample.png)
<br>

- **Media SVG Text**
![Media SVG](files_to_turn_in/mediaexamplesvg.png)
<br>

## Sololearn

- **Sololearn**
![SoloLearn](files_to_turn_in/sololearn2.png)
<br>