# Activity 5 Sheet
<b>Name:</b> Andriy Konyshev <br>
<b>Date:</b> 20240526 <br>
<b>Class:</b> CST-120 <br>
<b>Professor:</b> Bobby Estey <br><br>


## Screenshots


- **Part 1 Java** <br>
![Class Matrix](files_to_turn_in/part1.png)
<br>

- **Part 2 Java** <br>
![table 1-3](files_to_turn_in/part2.png)
<br>

- **Part 3 Java** <br>
![table 1-3](files_to_turn_in/part3.png)
<br>

## Sololearn

- **Sololearn** <br>
![SoloLearn](files_to_turn_in/sololearn.png)
<br>