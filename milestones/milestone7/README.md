# Milestone 7
<b>Name:</b> Andriy Konyshev <br>
<b>Date:</b> 20240609 <br>
<b>Class:</b> CST-120 <br>
<b>Professor:</b> Bobby Estey <br><br>


### This file will document the development and screenshots of Milestone 4.


# Sitemap
- This picture shows the websites that are accessible. There are now 5 websites. The main page, the project page, contacts page, anomaly list page, and a media page.<br>

![Sitemap](turn_in_media/Sitemap2.0.png)<br>

# Wireframe
- This shows the small layouts of the pages and what the user should expect to see.<br>
![Wireframe](turn_in_media/Wireframes2.0.png)<br>

# Website Screenshots


![Form](turn_in_media/form.png) <br>
- Created a form to submit for contact. Notice how the email section does not allow the user to continue without submitting an actual email. <br>


![postman](turn_in_media/Code.png)<br>
- Code from submitting the file.  <br>





# Screencap
- This is the video showing the websites functionality. Code is shown.<br>

![Video](turn_in_media/media.mp4)


# Conclusion

- Due to how busy I am, I am not happy with the website on a personal level. What was done was to fulfill the requirements needed for the assignment. The time spent to create just one design will be noted for future classes and will reflect on the schedule, however, for what it is right now if I was to ever make a website for myself, this will only be a basic reference.