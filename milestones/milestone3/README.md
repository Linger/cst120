# Milestone 3
<b>Name:</b> Andriy Konyshev <br>
<b>Date:</b> 20240512 <br>
<b>Class:</b> CST-120 <br>
<b>Professor:</b> Bobby Estey <br><br>


### This file will document the development and screenshots of Milestone 3.


# Sitemap
- This picture shows the websites that are accessible. There are now 5 websites. The main page, the project page, contacts page, anomaly list page, and a media page.<br>

![Sitemap](turn_in_media/Sitemap2.0.png)

# Wireframe
- This shows the small layouts of the pages and what the user should expect to see.<br>
![Wireframe](turn_in_media/Wireframes2.0.png)

# Website Screenshots


![Table Edit](turn_in_media/table.png)

- This page already utilized a lot of CSS properties, however, I managed to move all of them to one external CSS files so it is available for re-use. The table was also edited to look more neat. <br> 
- All sources are in the "link" section.<br>

![Contacts animation](turn_in_media/contactsanimation.png)

- Since this page will have the least amount of content, I added a little transitioning animation for the "Contacts" title, just so it does not look as blank<br>




# Screencap
- This is the video showing the websites functionality. Code is shown.

![Video](turn_in_media/media.mp4)


# Conclusion

The website has not changed much other than to meet some of the requirements like the table and an animation due to the fact that I utilized CSS before this, meeting the CSS requirements for this milestone. External CSS was also implemented so it is easier to expand the website later.