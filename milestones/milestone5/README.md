# Milestone 5
<b>Name:</b> Andriy Konyshev <br>
<b>Date:</b> 20240526 <br>
<b>Class:</b> CST-120 <br>
<b>Professor:</b> Bobby Estey <br><br>


### This file will document the development and screenshots of Milestone 5.


# Sitemap
- This picture shows the websites that are accessible. There are now 5 websites. The main page, the project page, contacts page, anomaly list page, and a media page.<br>

![Sitemap](turn_in_media/Sitemap2.0.png)

# Wireframe
- This shows the small layouts of the pages and what the user should expect to see.<br>
![Wireframe](turn_in_media/Wireframes2.0.png)

# Website Screenshots


![welcome, and time](turn_in_media/picture1.png)
- Cleaned up HTML file and added logs to report. <br>

![scroll up button](turn_in_media/picture2.png)
- Created a scroll up button if your monitor resolution is small and if the website gets expanded. Can also be helpful on phones. <br>

![Random SCP](turn_in_media/picture3.png)
- When clicking on the text, it pops up an alert for a random SCP for you to look at. Can be easily expanded in the javascript file.<br>


![Button](turn_in_media/picture4.png)
- The useless button added console log and put in separate file.<br>




# Screencap
- This is the video showing the websites functionality. Code is shown/highlighted.<br>

![Video](turn_in_media/media.mp4)


# Conclusion

- Currently working on flex on a separate folder to have the links up top. Will be added tomorrow. Having trouble what to use Javascript for in terms of interactability for a planner website, will consult a friend for ideas.