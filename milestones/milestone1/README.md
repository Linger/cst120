# Milestone 1
By Andriy Konyshev


### This file will document the development and screenshots of Milestone 1.


# Sitemap
- This picture shows the websites that are accessible. There 3 websites. The main page, the project page, and the contacts page.

![Sitemap](https://gitlab.com/Linger/cst120/-/raw/main/milestones/milestone1/images/Sitemap.png?ref_type=heads)

# Wireframe
- This shows the small layouts of the pages and what the user should expect to see.
![Wireframe](https://gitlab.com/Linger/cst120/-/raw/main/milestones/milestone1/images/Wireframes.png?ref_type=heads)

# About Me
- This is the main page with "about me" layout. It also leads to other pages.
![Wireframe](https://gitlab.com/Linger/cst120/-/raw/main/milestones/milestone1/images/Aboutmemain.png?ref_type=heads)

# Screencap
- This is the video showing the websites functionality.

[![WebVideo](https://markdown-videos-api.jorgenkh.no/youtube/JiOWphvcacg)](https://youtu.be/JiOWphvcacg)

