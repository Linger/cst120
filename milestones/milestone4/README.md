# Milestone 4
<b>Name:</b> Andriy Konyshev <br>
<b>Date:</b> 20240519 <br>
<b>Class:</b> CST-120 <br>
<b>Professor:</b> Bobby Estey <br><br>


### This file will document the development and screenshots of Milestone 4.


# Sitemap
- This picture shows the websites that are accessible. There are now 5 websites. The main page, the project page, contacts page, anomaly list page, and a media page.<br>

![Sitemap](turn_in_media/Sitemap2.0.png)

# Wireframe
- This shows the small layouts of the pages and what the user should expect to see.<br>
![Wireframe](turn_in_media/Wireframes2.0.png)

# Website Screenshots


![Alert](turn_in_media/prompt.png)
- Alert that pops up when you access website. <br>


![Contacts animation](turn_in_media/timeandwelcome.png)
- Some of the things that pop up in debug<br>


![Contacts animation](turn_in_media/timewebsite.png)
- In here is shown the before/after javascript with the welcoming. On the bottom you can see the accessed website time.<br>




# Screencap
- This is the video showing the websites functionality. Code is shown.<br>

![Video](turn_in_media/media.mp4)


# Conclusion

- The website has not been fully worked on due to a busy schedule, however, this week the website will be reworked for proper presentation. Flex will be added for links, different coloration to not tire out the eyes and bring a users attention to different links, while also thinking on what to add with Javascript.