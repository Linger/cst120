# Milestone 6
<b>Name:</b> Andriy Konyshev <br>
<b>Date:</b> 20240602 <br>
<b>Class:</b> CST-120 <br>
<b>Professor:</b> Bobby Estey <br><br>


### This file will document the development and screenshots of Milestone 4.


# Sitemap
- This picture shows the websites that are accessible. There are now 5 websites. The main page, the project page, contacts page, anomaly list page, and a media page.<br>

![Sitemap](turn_in_media/Sitemap2.0.png)<br>

# Wireframe
- This shows the small layouts of the pages and what the user should expect to see.<br>
![Wireframe](turn_in_media/Wireframes2.0.png)<br>

# Website Screenshots


![Table](turn_in_media/tableboot.png) <br>
- New variation of the table. <br>


![Links](turn_in_media/mainboot.png)<br>
- Main pages new header for the links. Stays the same across all websites except for one at the moment. <br>


![FAQ](turn_in_media/faqboot.png)<br>
- Added a hide/reveal FAQ for the user. Clicking on it changes the color and reveals text hidden underneath.<br>




# Screencap
- This is the video showing the websites functionality. Code is shown.<br>

![Video](turn_in_media/media.mp4)


# Conclusion

- Added a FAQ, a better header for the links on top, also fixed the visuals of the "scroll to top" button where instead of teleporting, it slowly scrolls you up. Another thing that was re-done was the table in the anomalylist page. This will allow for easier editing of the table visuals in the future. Resources used were from getbootstrap.com