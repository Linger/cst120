# Milestone 2
<b>Name:</b> Andriy Konyshev <br>
<b>Date:</b> 20240502 <br>
<b>Class:</b> CST-120 <br>
<b>Professor:</b> Bobby Estey <br><br>


### This file will document the development and screenshots of Milestone 2.


# Sitemap
- This picture shows the websites that are accessible. There are now 5 websites. The main page, the project page, contacts page, anomaly list page, and a media page.

![Sitemap](turn_in_media/Sitemap2.0.png)

# Wireframe
- This shows the small layouts of the pages and what the user should expect to see.
![Wireframe](turn_in_media/Wireframes2.0.png)

# Website Screenshots
- This is the main page with "about me" layout. It also leads to other pages. It only got more links added to the page so the user can reach them quicker if they visited this website before.

![Wireframe](turn_in_media/Aboutmemain.png)

- This is one of the new pages which contains a list of SCPs that I personally found interest and a bit inspiring to read when I try to make a story myself. I tried a bit of a different layout considering the "Foundation" is a dark organization. 
- All sources are in the "link" section.

![Wireframe](turn_in_media/anomaly_list.png)

- This is the last of the new pages. This is the media page. It contains a youtube video to complete the requirement for a video, and an audio clip I took out of a video. All sources are on the website.

![Wireframe](turn_in_media/mediapage.png)


# Screencap
- This is the video showing the websites functionality.

![Video](turn_in_media/recordmilestone2.mp4)


# Conclusion

Currently need to think of a new project idea that I am willing to publicly share, otherwise that will stay empty. This milestone was a good way to solidify the understanding of using more internal CSS, tables, and how to add video/audio in a simple manner. 